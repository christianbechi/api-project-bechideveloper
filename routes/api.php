<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\v1\customer\CustomerController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::prefix('v1')->namespace('Api\v1')->group(function () {
        Route::resource('/customers', 'customer\CustomerController');
        Route::resource('/recordings', 'recording\RecordingController');
        Route::resource('/dishes', 'dish\DishController');
        Route::get('/userVideo/{id}', 'customer\AdvertisementController@getUserVideo');
    } 
);