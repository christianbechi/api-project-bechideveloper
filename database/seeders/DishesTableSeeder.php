<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class DishesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dishes')->insert([
            [
                'name' => 'Tilápia du chef',
                'recording_video_path' => 'C:\Users\Christian-Bechi\Videos\tilapia_Chef',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Filé de pintado ao molho',
                'recording_video_path' => 'C:\Users\Christian-Bechi\Videos\file_pintado_molho',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Escondidinho',
                'recording_video_path' => 'C:\Users\Christian-Bechi\Videos\Escondidinho',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Carne de sal cremosa',
                'recording_video_path' => 'C:\Users\Christian-Bechi\Videos\carne_sal_cremosa',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Galinhada',
                'recording_video_path' => 'C:\Users\Christian-Bechi\Videos\galinhada',
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ]);
    }
}
