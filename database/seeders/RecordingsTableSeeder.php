<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RecordingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('recordings')->insert([
            [
                'name' => 'Angélica',
                'recording_name_path' => 'C:\Users\Christian-Bechi\Music\angelica.mp3',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Christian',
                'recording_name_path' => 'C:\Users\Christian-Bechi\Music\christian.mp3',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Raíssa',
                'recording_name_path' => 'C:\Users\Christian-Bechi\Music\raissa.mp3',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Pablo',
                'recording_name_path' => 'C:\Users\Christian-Bechi\Music\pablo.mp3',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Antero',
                'recording_name_path' => 'C:\Users\Christian-Bechi\Music\antero.mp3',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Ayani',
                'recording_name_path' => 'C:\Users\Christian-Bechi\Music\ayani.mp3',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Marcos',
                'recording_name_path' => 'C:\Users\Christian-Bechi\Music\marcos.mp3',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Wilson',
                'recording_name_path' => 'C:\Users\Christian-Bechi\Music\wilson.mp3',
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
    }
}
