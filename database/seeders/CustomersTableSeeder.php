<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert([
            [
                'name_customer' => 'Ayani',
                'customer_image_path' => 'C:\Users\Christian-Bechi\Pictures',
                'favorite_dish_id' => 1,
                'name_recording_id' => 6,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name_customer' => 'Cartman',
                'customer_image_path' => 'C:\Users\Christian-Bechi\Pictures',
                'favorite_dish_id' => 3,
                'name_recording_id' => 2,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name_customer' => 'Maximus',
                'customer_image_path' => 'C:\Users\Christian-Bechi\Pictures',
                'favorite_dish_id' => 5,
                'name_recording_id' => 1,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name_customer' => 'Pietra',
                'customer_image_path' => 'C:\Users\Christian-Bechi\Pictures',
                'favorite_dish_id' => 4,
                'name_recording_id' => 3,
                'created_at' => now(),
                'updated_at' => now(),
            ],  
            [
                'name_customer' => 'Hélder',
                'customer_image_path' => 'C:\Users\Christian-Bechi\Pictures',
                'favorite_dish_id' => 2,
                'name_recording_id' => 4,
                'created_at' => now(),
                'updated_at' => now(),
            ],  
            [
                'name_customer' => 'Arianna',
                'customer_image_path' => 'C:\Users\Christian-Bechi\Pictures',
                'favorite_dish_id' => 1,
                'name_recording_id' => 8,
                'created_at' => now(),
                'updated_at' => now(),
            ],  
            [
                'name_customer' => 'Fernanda',
                'customer_image_path' => 'C:\Users\Christian-Bechi\Pictures',
                'favorite_dish_id' => 5,
                'name_recording_id' => 7,
                'created_at' => now(),
                'updated_at' => now(),
            ],  
            [
                'name_customer' => 'Fernanda',
                'customer_image_path' => 'C:\Users\Christian-Bechi\Pictures',
                'favorite_dish_id' => 5,
                'name_recording_id' => 5,
                'created_at' => now(),
                'updated_at' => now(),
            ], 
            [
                'name_customer' => 'Dominique',
                'customer_image_path' => 'C:\Users\Christian-Bechi\Pictures',
                'favorite_dish_id' => 4,
                'name_recording_id' => 7,
                'created_at' => now(),
                'updated_at' => now(),
            ], 
            [
                'name_customer' => 'Kenny',
                'customer_image_path' => 'C:\Users\Christian-Bechi\Pictures',
                'favorite_dish_id' => 5,
                'name_recording_id' => 3,
                'created_at' => now(),
                'updated_at' => now(),
            ], 
        ]);
    }
}
