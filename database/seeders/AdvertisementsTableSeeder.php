<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdvertisementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('advertisements')->insert([
            [
                'customer_id' => 1,
                'customer_video_url' => "video",
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ]);
    }
}
