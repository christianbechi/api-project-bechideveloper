<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->string("name_customer");
            $table->string("customer_image_path");
            $table->unsignedBigInteger("favorite_dish_id");
            $table->unsignedBigInteger("name_recording_id");
            $table->timestamps();
            $table->foreign('favorite_dish_id')->references('id')->on('dishes')->onDelete("cascade");
            $table->foreign('name_recording_id')->references('id')->on('recordings')->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
