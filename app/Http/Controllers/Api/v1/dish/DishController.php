<?php

namespace App\Http\Controllers\api\v1\dish;

use App\Http\Controllers\Controller;
use App\Models\Dishes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

//"getID3\\": "vendor/james-heinrich/getid3/getid3"

class DishController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(Dishes $dishes)
    {
        $this->dishes = $dishes;
    }


    public function index()
    {
        try {
            $dishes = $this->dishes->all();
            if(!empty($dishes->all())){
                return $this->outputJSON($dishes, "false", "", 200);
            }else{
                return $this->outputJSON("", "false", "Não existe pratos cadastrados!", 404);
            }
        } catch (\Exception $e) {
            $this->outputJSON("", "true", "Erro ao trazer a lista dos clientes.", 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validate = Validator::make($request->all(), [
                "name" => "required",
                "recording_video" => "required"
            ],[
                "required" => "O campo :attribute não pode estar vazio!"
            ]);

            if($validate->fails())
                return $this->outputJSON("", "true", $validate->errors()->first(), 500);

            $videoFile = $request->file("recording_video");

            $responseValidate = $this->dishes->validateVideo($videoFile);
            if(!is_null($responseValidate))
                return $this->outputJSON("", "true", $responseValidate["message"], $responseValidate["code"]);

            $formatedName = explode(".",$videoFile->getClientOriginalName());
            $fileName = str_replace(" ","",$formatedName[0])."_".date("d-m-Y_H-i-s");

            if($formatedName[1] != "mp4")
                $this->dishes->convertToMp4($videoFile, $fileName, $formatedName);
            else
                $videoFile->move("storage/dish_presentation", $fileName.".".$formatedName[1]);

            $mountedURL = $this->dishes->mountURI($fileName);

            $dish = $this->dishes->firstOrCreate([
                "name" => $request->name,
                "recording_video_path" => $mountedURL
            ]);
            
            if($dish){
                return $this->outputJSON($dish,"false","",200);
            }else{
                return $this->outputJSON("", "true", "Erro ao tentar cadastrar esse prato!", 500);
            }

        } catch (\Exception $e) {
           return $this->outputJSON("", "true", "Não foi possivel cadastrar esse prato!".$e->getMessage(), 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $dish = $this->dishes->findOrFail($id);
            if($dish){
                return $this->outputJSON($dish,"false","",200);
            }
        } catch (\Exception $e) {
            return $this->outputJSON("", "false", "Esse prato não está cadastrado no sistema!",404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $dish = $this->dishes->findOrFail($id);
            if($dish->delete()){
                return $this->outputJSON($dish, "false", "Prato deletado com sucesso!", 200);
            }else{
                return $this->outputJSON("", "true", "Ocorreu um erro ao deletar a gravação!", 500);
            }
        } catch (\Exception $e) {
            return $this->outputJSON("", "false", "Esse prato não está cadastrada no sistema!", 404);
        }
    }
}
