<?php

namespace App\Http\Controllers\Api\v1\customer;

use App\Http\Controllers\Controller;
use App\Models\Advertisements;
use Illuminate\Http\Request;

class AdvertisementController extends Controller
{
    public function __construct(Advertisements $advertisements)
    {
        $this->advertisement = $advertisements;
    }

    public function getUserVideo(Request $request)
    {
        try {
            $dataUserVideo = $this->advertisement->with('customers')->where('customer_id',$request->id)->first();
            if(isset($dataUserVideo)){
                return $this->outputJSON($dataUserVideo, "false", "", 200);
            }else{
                return $this->outputJSON("","true","Ops, não foi possivel buscar o seu video!",500);
            }
        } catch (\Exception $e) {
            return $this->outputJSON("","true",$e->getMessage(),500);
        }
    }
}
