<?php

namespace App\Http\Controllers\api\v1\customer;

use App\Http\Controllers\Controller;
use App\Models\Customers;
use App\Models\Recordings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(
        Customers $customers, 
        Recordings $recordings)
    {
        $this->customers = $customers;
        $this->recordings = $recordings;
    }

    public function index()
    {
        try {
            $customers = $this->customers->all();
            if(!empty($customers->all())){
                return $this->outputJSON($customers,"false","",200);
            }else{
                return $this->outputJSON("","false","Não existe clientes cadastrados!",404);
            }
        } catch (\Exception $e) {
            return $this->outputJSON("","true","Erro ao trazer a lista dos clientes.",500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validate = Validator::make($request->all(), [
                "name_customer" => "required",
                "customer_image" => "required",
                "favorite_dish" => "required",
            ],[
                "required" => "O campo :attribute não pode estar vazio!"
            ]);

            if($validate->fails()){
                return $this->outputJSON("", "true", $validate->errors()->first(), 500);
            }

            $base64 = explode(",",$request->customer_image, 2);
            $fileExtension = explode(";",explode("/",$base64[0])[1])[0];
            $fileImage = $base64[1];
            $size = explode(".",(int)(strlen(rtrim($fileImage, '=')) * 0.75))[0];
            
            if($fileExtension != "png" && $fileExtension != "jpg" && $fileExtension != "jpeg"){
                return $this->outputJSON("","true","As unicas extenções permitidas são: JPG, JPEG e PNG!", 415);
            }else if($size > 5242880){
                return $this->outputJSON("","true","O tamanho maximo para o upload de imagem é de 5M");
            }
            $photoName = str_replace(" ","",$request->name_customer)."_".date("d-m-Y_H-i-s").".".$fileExtension;
            Storage::put("public/customer_photos/".$photoName, base64_decode($fileImage)); 
            
            $server = $_SERVER["HTTP_HOST"];
            $mountedURL = "https://".$server."/storage/customer_photos/".$photoName;
            
            $existVoiceRecording = $this->recordings->where("name",strtolower($request->name_customer))->first();

            if(!isset($existVoiceRecording))
                $existVoiceRecording = $this->recordings->generateVoice(strtolower($request->name_customer));

            $customer = $this->customers->firstOrCreate([
                "name_customer" => $request->name_customer,
                "customer_image_path" => $mountedURL,
                "favorite_dish_id" => $request->favorite_dish,
                "name_recording_id" => $existVoiceRecording->id,
            ]);

            $this->customers->generateVideo($customer->id,$request,$photoName);

            if($customer){
                return $this->outputJSON($customer, "false", "", 200);
            }else{
                return $this->outputJSON("","false","Não foi possivel cadastrar o usuario", 500);
            }
        } catch (\Exception $e) {
            return $this->outputJSON("","true",$e->getMessage(),500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $customer = $this->customers->findOrFail($id);
            if(isset($customer)){
                return $this->outputJSON($customer, "false", "", 200);
            }
        } catch (\Exception $e) {
            return $this->outputJSON("", "false", "Esse usuario não está cadastrado no sistema!", 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $customer = $this->customers->findOrFail($id);
            if($customer->delete()){
                return $this->outputJSON($customer, "false", "cliente deletado com sucesso!", 200);
            }else{
                return $this->outputJSON("", "true", "Ocorreu um erro ao deletar a gravação!", 500);
            }
        } catch (\Exception $e) {
            return $this->outputJSON("", "false", "Esse usuario não está cadastrado no sistema!", 404);
        }
    }
}
