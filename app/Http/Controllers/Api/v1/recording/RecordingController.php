<?php

namespace App\Http\Controllers\api\v1\recording;

use App\Http\Controllers\Controller;
use App\Models\Recordings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RecordingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(Recordings $recordings)
    {
        $this->recordings = $recordings;
    }

    public function index()
    {
        try {
            $recordings = $this->recordings->all();
            if(!empty($recordings->all())){
                return $this->outputJSON($recordings, "false", "", 200);
            }else{
                return $this->outputJSON("", "false", "Não existe gravações cadastradas!", 404);
            }
        } catch (\Exception $e) {
            return $this->outputJSON("","true","Erro ao trazer a lista das gravações.",500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validate = Validator::make($request->all(), [
                "name" => "required",
                "recording_name" => "required"
            ],[
                "required" => "O campo :attribute não pode estar vazio!"
            ]);

            if($validate->fails())
                return $this->outputJSON("", "true", $validate->errors()->first(), 500);

            $audioFile = $request->file("recording_name");

            
            $formatedName = explode(".",$audioFile->getClientOriginalName());
            $fileName = strtolower(str_replace(" ","",$formatedName[0]));

            $responseValidate = $this->recordings->validateAudio($audioFile,$fileName);
            if(!is_null($responseValidate))
                return $this->outputJSON("", "true", $responseValidate["message"], $responseValidate["code"]);

            if($formatedName[1] != "mp3")
                $this->recordings->convertToMp3($fileName, $formatedName, $audioFile);
            else
                $audioFile->move("storage/recordings", $fileName.".mp3");

            $mountedURL = $this->recordings->mountURI($fileName);

            $recording = $this->recordings->firstOrCreate([
                "name" => $fileName,
                "recording_name_path" => $mountedURL
            ]);

            if($recording){
                return $this->outputJSON($recording, "true", "", 200);
            }else{
                return $this->outputJSON("", "false", "Erro ao tentar cadastrar essa gravação!", 500);
            }

        } catch (\Exception $e) {
            return $this->outputJSON("", "true", "Não foi possivel cadastrar essa gravação!".$e->getMessage(), 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $recording = $this->recordings->findOrFail($id);
            if(isset($recording)){
                return $this->outputJSON($recording, "false", "", 200);
            }
        } catch (\Exception $e) {
            return $this->outputJSON("", "false", "Essa gravação não está cadastrada no sistema!",404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $recording = $this->recordings->findOrFail($id);
            if($recording->delete()){
                return $this->outputJSON($recording, "false", "Gravação deletada com sucesso!", 200);
            }else{
                return $this->outputJSON("", "true", "Ocorreu um erro ao deletar a gravação!", 500);
            }
        } catch (\Exception $e) {
            return $this->outputJSON("", "false", "Essa gravação não está cadastrada no sistema!", 404);
        }
    }
}
