<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function outputJSON($result = null, $error = null, $message = null, $responseCode = 200){
        if($error != null){
            $response["error"] = $error;
            $response["responseCode"] = $responseCode;
            $response["message"] = $message;
        }

        if($result != null){
            $response["data"] = $result;
        }

        return response()->json($response,$responseCode);
    }
}
