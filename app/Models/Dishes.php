<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use ProtoneMedia\LaravelFFMpeg\FFMpeg\FFProbe;
use ProtoneMedia\LaravelFFMpeg\Support\FFMpeg;

class Dishes extends Model
{
    use HasFactory;

    protected $fillable = [
        "id",
        "name",
        "recording_video_path",
        "created_at",
        "updated_at"
    ];

    public function customer(){
        return $this->hasMany(Customers::class,"favorite_dish_id");
    }

    public function validateVideo($videoFile){
            $ffprob = FFProbe::create();

            $videoLenght = $ffprob->format($videoFile)->get("duration");

            if(
                $videoFile->extension() != "mp4" && 
                $videoFile->extension() != "avi" && 
                $videoFile->extension() != "mov" &&
                $videoFile->extension() != "asf" &&
                $videoFile->extension() != "qt")
            {

                return ["message" => "As unicas extenções permitidas são: MP4, AVI, WMV e MOV!", "code" => 415];
                
            }else if($videoFile->getSize() > 157286400){
                
                return ["message" => "O tamanho maximo para o upload de video é de 150M!", "code" => 413];
                
            }else if($videoLenght > 120){
                
                return ["message" => "A duração maxima de um video deve ser de 2 Minutos!", "code" => 415];

            }

            return null;
    }

    public function convertToMp4($videoFile, $fileName, $formatedName){
        $videoFile->storeAs("tmp", $fileName.".".$formatedName[1]);

        FFMpeg::fromDisk("local")
            ->open("tmp\\".$fileName.".".$formatedName[1])
            ->export()
            ->toDisk("public")
            ->inFormat(new \FFMpeg\Format\Video\X264("libmp3lame"))
            ->save("dish_presentation\\".$fileName.".mp4");
                
        Storage::deleteDirectory("tmp");
    }

    public function mountURI($fileName){
        $server = $_SERVER["HTTP_HOST"];
        return "https://".$server."/storage/dish_presentation/".$fileName.".mp4";
    }
}
