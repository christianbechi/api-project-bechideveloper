<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Advertisements extends Model
{
    use HasFactory;

    protected $fillable = [
        "id",
        "customer_id",
        "customer_video_url",
        "created_at",
        "updated_at"
    ];

    public function customers(){
        return $this->belongsTo(Customers::class,'id');
    }
}
