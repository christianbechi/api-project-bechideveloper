<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use ProtoneMedia\LaravelFFMpeg\FFMpeg\FFProbe;
use ProtoneMedia\LaravelFFMpeg\Support\FFMpeg;
use Cion\TextToSpeech\Facades\TextToSpeech;

class Recordings extends Model
{
    use HasFactory;

    protected $fillable = [
        "id",
        "name",
        "recording_name_path",
        "created_at",
        "updated_at"
    ];


    public function customers(){
        return $this->belongsTo(Customers::class,"id","name_recording_id");
    }

    public function validateAudio($audioFile,$fileName){

        $ffprobe = FFProbe::create();
        
        $durationAudio = explode(".",$ffprobe->format($audioFile)->get("duration"))[0];
        $hasAudio = Recordings::where('name',$fileName)->first();

        if($audioFile->getSize() > 10485760){
            return ["message" => "O tamanho maximo para o upload de audio é de 10M!", "code" => 413];
        }else if($durationAudio > 5){
            return ["message" => "A duração maxima de um audio deve ser de até 5 segundos!", "code" => 415];
        }else if(isset($hasAudio)){
            return ["message" => "Já existe esse audio cadastrado!", "code" => 500];
        }

        return null;
    }

    public function convertToMp3($fileName, $formatedName, $audioFile){
        $audioFile->storeAs("tmp",$fileName.".".$formatedName[1]);
        FFMpeg::fromDisk("local")
        ->open("tmp\\".$fileName.".".$formatedName[1])
        ->export()
        ->toDisk("public")
        ->inFormat(new \FFMpeg\Format\Audio\Mp3)
        ->save("recordings\\".$fileName.".mp3");

        Storage::deleteDirectory("tmp");
    }

    public function generateVoice($fileName){
        
        TextToSpeech::disk("public")
        ->saveTo("recordings\\".$fileName.".mp3")
        ->convert($fileName);

        $namePath = $this->mountURI($fileName);

        $recording = Recordings::firstOrCreate([
            "name" => $fileName,
            "recording_name_path" =>  $namePath
        ]);

        return $recording;
    }

    public function mountURI($fileName){
        $server = $_SERVER["HTTP_HOST"];
        return "https://".$server."/storage/recordings/".$fileName.".mp3";
    }
}
