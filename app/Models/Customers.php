<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use ProtoneMedia\LaravelFFMpeg\Filesystem\Media;
use ProtoneMedia\LaravelFFMpeg\Support\FFMpeg;

class Customers extends Model
{
    use HasFactory;

    protected $fillable = [
        "id",
        "name_customer",
        "customer_image_path",
        "favorite_dish_id",
        "name_recording_id",
        "created_at",
        "updated_at"
    ];

    public function dish(){
        return $this->hasOne(Dishes::class,"id","favorite_dish_id");
    }

    public function recording(){
        return $this->hasOne(Recordings::class,"id", "name_recording_id");
    }

    public function advertisements(){
        return $this->hasMany(Advertisements::class,'id');
    }

    public function generateVideo($customer_id, $requestData, $image){
        $nameDish = explode('/',Dishes::find($requestData->favorite_dish)->recording_video_path)[5];
        $advertisementFileName = strtolower('advertisements_'.$requestData->name_customer.'_'.date("d-m-Y_H-i-s").'.mp4');

        if(isset($nameDish)){
            FFMpeg::fromDisk('local')
            ->open(['assets\\comercial.mp4','assets\\comercial.mp3'])
            ->fromDisk('public')
            ->open(['recordings\\'.strtolower($requestData->name_customer).'.mp3','customer_photos\\'.$image,'dish_presentation\\'.$nameDish])
            ->export()
            ->addFilter('[1:a]','volume=3.0','[a1]')
            ->addFilter('[2:a]','adelay=7400|7400,volume=5.0','[a2]')
            ->addFilter('[a1][a2]','amix=inputs=2','[audio]')
            ->addFilter('[3:v]','format=rgba,scale=150:150, rotate=0:c=00000000:ow=rotw(iw):oh=roth(ih), perspective=-8:0:W:0:0:165:192:185','[3v]')
            ->addFilter('[4:v]',"format=rgba, scale=170:110, rotate=0:c=0x00000000:ow=rotw(iw):oh=roth(ih), perspective=-10:-8:W:-3:0:100:W:100",'[4v]')
            ->addFilter('[0:v][3v]',"overlay=main_w-(overlay_w+190):main_h-(70+overlay_h):enable='between(t,13,45)'",'[video]')
            ->addFilter('[video][4v]',"overlay=70:180:shortest=1:enable='between(t,20,48)'",'[complete]')
            ->addFormatOutputMapping(
                new \FFMpeg\Format\Video\X264('aac'), 
                Media::make('public', 'advertisements/'.$advertisementFileName),
                ['[complete]','[audio]']
            )
            ->save();

            $server = $_SERVER["HTTP_HOST"];
            $url = "https://".$server."/storage/advertisements/".$advertisementFileName;

            Advertisements::firstOrCreate([
                "customer_id" => $customer_id,
                "customer_video_url" => $url
            ]);
        }
    }
}
